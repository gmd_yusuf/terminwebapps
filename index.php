<?php

/*

BLR Terminvorschau
Copyright 2020 NexCast GmbH

Written by Justin Junkes

*/

$page = $_GET['page'];

if(!isset($_GET['page'])){
    header("Location: /dashboard");
} 

// Include Functions
include_once 'core/functions.php';

// HTML Head Include
include_once 'includes/head.php';

// Nav
include_once 'includes/nav.php';

// Load Inner Page
include_once 'pages/'.$page.'.php';


// Footer Include
include_once 'includes/footer.php';


?>