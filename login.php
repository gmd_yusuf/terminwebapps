<?php



/*

BLR Terminvorschau
Copyright 2020 NexCast GmbH

Written by Justin Junkes

*/

// Include Functions
include_once 'core/functions.php';

// HTML Head Include
include_once 'includes/head-login.php';

// Nav
include_once 'includes/nav-login.php';

// Load Inner Page
include_once 'pages/login.php';


// Footer Include
include_once 'includes/footer.php';

?>