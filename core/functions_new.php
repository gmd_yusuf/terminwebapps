<?php

session_start();

// Database Config
include_once 'config.php';

function get_users_select(){
    global $pdo;

    $get = $pdo->prepare("SELECT * FROM users WHERE editor = 'true' ORDER by surname ASC");
    $get->execute();

    while($data = $get->fetch()){
        $user_id = $data['id'];
        $username = $data['surname'].' '.$data['name'];
        echo '<option value="'.$user_id.'">'.$username.'</option>';
    }
}

function get_types_select(){
    global $pdo;

    $get = $pdo->prepare("SELECT * FROM types ORDER by 'name' ASC");
    $get->execute();

    while($data = $get->fetch()){
        $type_id = $data['id'];
        $name = $data['name'];
        echo '<option value="'.$type_id.'">'.$name.'</option>';
    }
}

function get_categories_select(){
    global $pdo;

    $get = $pdo->prepare("SELECT * FROM category ORDER by 'name' ASC");
    $get->execute();

    while($data = $get->fetch()){
        $type_id = $data['id'];
        $name = $data['name'];
        echo '<option value="'.$type_id.'">'.$name.'</option>';
    }
}




// Session Service

// Login
if(isset($_POST['logmein']))
{
	$U = $_POST['mail'];
	$P = $_POST['password'];
	$Fail = false;
	
    $GetUser = $pdo->prepare("SELECT * FROM users WHERE mail = '$U' AND password = '".md5($P)."'");
    $GetUser->execute();
    $GetUser_success = $GetUser->rowCount();
	if($GetUser_success =='0')
	{
		echo '<div class="alert alert-danger" role="alert">
        <strong>Oh nein! </strong>Der Login war nicht erfolgreich. User existiert nicht
      </div>';
		$Fail = true;
    }
	if($Fail == false)
	{
		if($GetUser_success > 0)
		{
            $_SESSION['mail'] = $U;
            $_SESSION['password'] = $P;
		}
	}
}

if(isset($_SESSION['mail']) && isset($_SESSION['password']))
{
	$SU = $_SESSION['mail'];
    $SP = $_SESSION['password'];
    $GetUser = $pdo->prepare("SELECT * FROM users WHERE mail = '$SU' AND password = '".md5($SP)."'");
    $GetUser->execute();
    $GetUser_success = $GetUser->rowCount();
	if($GetUser_success > 0)
	{
		$blr_user = $GetUser->fetch();
		define("User", true);
	}
	
} else {
	define("User", false);
}


function get_user_name($id){
    global $pdo;

    $get = $pdo->prepare("SELECT * FROM users WHERE id = $id LIMIT 1");
    $get->execute();

    while($data = $get->fetch()){
        return $data['surname'].' '.$data['name'];
    }
}

function get_type($id){
    global $pdo;

    $get = $pdo->prepare("SELECT * FROM types WHERE id = $id LIMIT 1");
    $get->execute();

    while($data = $get->fetch()){
        $type = $data['name'];
        return $type;
    }
}

function get_category($id){
    global $pdo;

    $get = $pdo->prepare("SELECT * FROM category WHERE id = $id LIMIT 1");
    $get->execute();

    while($data = $get->fetch()){
        return $data['name'];
    }
}

function get_public_status($status){
    if($status == 'true'){
        return '<i class="fas fa-2x fa-globe-europe" style="color:green" title="Öffentlich"></i>';
    }elseif($status == 'false'){
        return '<i class="fa fa-2x fa-globe-europe" style="color:red" title="Nicht öffentlich"></i>';
    }
}

function get_approve_status($status){
    if($status == 'true'){
        return '<i class="fa fa-2x fa-user-check" style="color:green" title="Freigegeben"></i>';
    }elseif($status == 'false'){
        return '<i class="fa fa-2x fa-user-times" style="color:red" title="Nicht freigegeben"></i>';
    }
}

function dashboard_view(){
    global $pdo;
    global $trans;
    global $blr_user;

    $timestamp_now = time();
    $today = strtotime('-1 day', $timestamp_now);
   

    $get = $pdo->prepare("SELECT * FROM termine WHERE datum >= $today AND trash != '1' ORDER BY datum ASC");
    $get->execute();

    while($data = $get->fetch()){  
        $id = $data['id'];
        $creator = get_user_name($data['creator']);
        $assigned_to = get_user_name($data['assignation']);
        $infotext = $data['text'];
        $stand = $data['bearbeitungsstand'];

    
        if($infotext != '&lt;br&gt;'){
            $text_button = ' <i class="far fa-2x fa-caret-square-down" title="Mehr anzeigen"></i>';
            $text_toogle = 'data-toggle="collapse" data-target="#termin'.$id.'" class="accordion-toggle"';
            $text_text = '<tr >
            <td colspan="6" class="hiddenRow"><div class="accordian-body collapse" id="termin'.$id.'">
            
              <div class="col-lg-12">'.html_entity_decode($infotext).'</div>
            </div> </td>
        </tr>';
        }else{
            $text_button = '';
            $text_toogle = '';
            $text_text = '';
        }
    

        $get_type = explode(',', $data['art']);
        foreach ($get_type as $type_id){
           $type = get_type($type_id);
        }


        $category = get_category($data['category']);
        $public_status = $data['public'];
        $approved_status = $data['approved'];
        $datum = $data['datum'];
        if($public_status == 'true' && $approved_status == 'true'){
            $show_status = '<i class="fa fa-2x fa-check-double" style="color:green" title="Öffentlich & Freigegeben"></i>';
        }else{
            $show_status = get_public_status($public_status).' '.get_approve_status($approved_status);
        }
        
        $get_date = $data['datum'];
        $full_date = date('d.m.Y H:i', $get_date);
        $date = date('d.m.Y', $data['datum']);
        $time = date('H:i', $data['datum']);
        if($time == '00:00'){
            $timevalue = strtr(date("l", $get_date), $trans).', '.date('d.m.Y', $get_date).'';
        }else{
            $timevalue = strtr(date("l", $get_date), $trans).', '.date('d.m.Y H:i', $get_date).' Uhr';
        }
      
        echo '
        <tr '.$text_toogle.'>
          <td>
         
          '.bearbeitungsstand($stand).'
          '.$show_status.'
          '.$text_button.'</td>
            <td>'.$timevalue.'</td>
            <td>'.$data['title'].'</td>
            <td>'.$type.' in '.$category.'</td>
            <td>'.$creator.'</td>
            <td>'.$assigned_to.'</td>
            <td>
                <a href="/edit/'.$id.'"><button class="btn btn-warning"><i class="fa fa-edit"></i></button></a>
                ';
                if($blr_user['is_admin'] == 'true'){
                echo '<a href="/delete/'.$id.'"><button class="btn btn-danger"><i class="fa fa-trash"></i></button></a>';
                }else{
                    // nothing
                }
                echo'
            </td>
        </tr>
        
        '.$text_text.'';

    }
}

function public_view(){
    global $pdo;
    global $trans;

    $timestamp_now = time();
    $today = strtotime('-1 day', $timestamp_now);
    $max_time = strtotime('+13 day', $timestamp_now);
    

    $get = $pdo->prepare("SELECT * FROM termine WHERE datum >= $today AND datum <= $max_time AND approved = 'true' AND public = 'true' AND trash != '1' ORDER BY datum ASC");
    $get->execute();

    while($data = $get->fetch()){  
        $id = $data['id'];
        $get_type = $data['art'];
        if($get_type == 4){
            $type = '';
        }else{
        $type = get_type($get_type);
        }
        $category = get_category($data['category']);
        $datum = $data['datum'];
        $title = $data['title'];
        
        
        echo '
            <div class="mt-3">
                <div class="card">
                    <div class="card-header text-center">
                    <p>'. strtr(date("l", $datum), $trans).', '.date('d.m.Y', $datum).'</p>
                    <p><b>'.$title.'</b></p> 
                    <p><span class="badge badge-primary">'.$type.'</span> <span class="badge badge-info">'.$category.'</span></p>
                    </div>

                    
                    
                       
                </div>
            </div>
        ';
        /*
        Table View
        echo '
        <tr data-toggle="collapse" data-target="#termin'.$id.'" class="accordion-toggle">
          <td><i class="far fa-2x fa-caret-square-down" title="Mehr anzeigen"></i>
          '.$show_status.'</td>
            <td>'.date('d.m.Y H:i', $data['datum']).'</td>
            <td>'.$data['title'].'</td>
            <td>'.$type.' in '.$category.'</td>
            <td>'.$creator.'</td>
            <td>'.$assigned_to.'</td>
            <td>
                <button class="btn btn-warning"><i class="fa fa-edit"></i></button> 
                <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
               
            </td>
        </tr>
        <tr >
            <td colspan="6" class="hiddenRow"><div class="accordian-body collapse" id="termin'.$id.'">
            
              <div class="col-lg-12">'.$data['text'].'</div>
            </div> </td>
        </tr>
        ';
        */

    }
}

function audioshop(){
    global $pdo;
    global $trans;

    $timestamp_now = time();
 
    $i = 1; $i <= 10;

    while($row = mysql_fetch_assoc($query)) {
    // Your code

    $i++;
}
}


if(isset($_POST['add_termin'])){
    $art = implode( ',', $_POST['art']);
    add_termin($_POST['date'],$_POST['title'],htmlspecialchars($_POST['text']),$_POST['assignation'],$art,$_POST['public'],$_POST['approved'],$_POST['category']
);
}

function add_termin($datum,$title,$text,$assignation,$art,$public,$approved,$category){
    global $pdo;
    global $blr_user;

    $add_datum = strtotime($datum);
    $add_title = $title;
    $add_text = $text;

    $add_creator = $blr_user['id'];
    $add_assignation = $assignation;
    $add_art = $art;
    $add_public = $public;
    $add_approved = $approved;
    $add_category = $category;

    //$add = $pdo->prepare("INSERT INTO termine SET datum = '$add_datum', title = '$add_title', text = '$add_text', creator = '$add_creator', assignation = '$add_assignation', art = '$add_art', public = '$add_public', approved = '$add_approved', category = '$add_category'");
    //$add->execute();
    //$text = $text."<";

    $text_combined = "<!doctype html><html><head><title>x</title></head><body>".$text."</body></html>";// add this code have purpose to mark this is a html code
    $validate = validatehtml($text_combined);
    if(trim($validate) == ''){// if no errors
        

        $add = $pdo->prepare("INSERT INTO termine SET datum = '$add_datum', title = '$add_title', text = '$add_text', creator = '$add_creator', assignation = '$add_assignation', art = '$add_art', public = '$add_public', approved = '$add_approved', category = '$add_category'");
        $add->execute();
        echo '<div class="alert alert-primary" role="alert">'."<strong>Insert data success.</strong>".
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button></div>';
    }else{
        echo '<div class="alert alert-danger" role="alert">'."<strong>Insert data failed.</strong><br/>".$validate.
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button></div>';
    }


}

function bearbeitungsstand($value){
    switch ($value) {
        case 0:
            return '<i class="fas fa-2x fa-exclamation-circle" style="color:red"></i>';
            break;

        case 1:
            return '<i class="fa fa-2x fa-dot-circle" style="color:orange"></i>';
            break;

        case 2:
            return '<i class="fa fa-2x fa-check-circle" style="color:green"></i>';
            break;
        
        default:
            # code...
            break;
    }
}

function bearbeitungsstand_edit($value){
    switch ($value) {
        case 0:
            return 'Offen';
            break;

        case 1:
            return 'In Bearbeitung';
            break;

        case 2:
            return 'Erledigt';
            break;
        
        default:
            # code...
            break;
    }
}


function getEditView($id){
    global $pdo;
    global $blr_user;

    $get_data = $pdo->prepare("SELECT * FROM termine WHERE id = $id LIMIT 1");
    $get_data->execute();

    while($data = $get_data->fetch()){

        $date = date('d-m-Y H:i', $data['datum']);
        $assignation = $data['assignation'];
        $category = $data['category'];
        $freigabe = $data['approved'];
        $public_app = $data['public'];
        $stand = $data['bearbeitungsstand'];


        echo '<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="container">
                <div class="card mt-5">
                    <div class="card-body">
                        <form action="/dashboard" method="post">
                            <div class="form-group">
                                <label for="title">
                                Titel
                                </label>
                                <input type="text" class="form-control" name="title" id="title" value="'.$data['title'].'">
                            </div>

                            <div class="form-group">
                            <label for="datetimepicker">Datum</label>
                <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" name="date" id="datetimepicker" data-toggle="datetimepicker" data-target="#datetimepicker" value="'.$date.'"/>
                   
                </div>
            </div>

                            <div class="form-group">
                                <label for="assignation">Zuweisung</label>
                                <select name="assignation" class="form-control" id="assignation">
                                <option value="'.$assignation.'" selected>'.get_user_name($assignation). ' (aktuell)</option>
                                ';
                                echo get_users_select();
                                echo '
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="assignation">Kategorie</label>
                                <select name="category" class="form-control" id="category">
                                <option value="'.$category.'" selected>'.get_category($category). ' (aktuell)</option>
                                ';
                                echo get_categories_select();;
                                echo '
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="text">Termindetails</label>
                                <textarea name="text" id="text" style="width: 100%;height: 300px;margin: 0;padding: 0;border-width: 0; ">'.html_entity_decode($data['text']).'</textarea>                            
                            </div>';

                            // nur admin Rechte
                            if($blr_user['is_admin'] == 'true'){
                                
                                echo '<div class="form-group">
                                <label for="approved">Freigabe</label>
                                <select name="approved" id="approved" class="form-control">
                                    <option value="'.$freigabe.'" selected>'.$freigabe.' (aktuell)</option>
                                    <option value="false">Nicht freigegeben</option>
                                    <option value="true">Freigegeben</option>
                                </select> 
                            </div>
                            <div class="form-group">
                                <label for="public">Öffentliche Anzeige</label>
                                <select name="public" id="public" class="form-control">
                                    <option value="'.$public_app.'" selected>'.$public_app.' (aktuell)</option>
                                    <option value="false">Nicht öffentlich</option>
                                    <option value="true">Öffentlich</option>
                                </select> 
                            </div>
                            <div class="form-group">
                                <label for="stand">Bearbeitungsstand</label>
                                <select name="stand" id="stand" class="form-control">
                                    <option value="'.$stand.'" selected>'.bearbeitungsstand_edit($stand).' (aktuell)</option>
                                    <option value="0">Offen</font></option>
                                    <option value="1">In Bearbeitung</i></option>
                                    <option value="2">Erledigt</i></option>
                                </select> 
                            </div>
                            ';
                                }else{
                                    echo '
                                    <input type="hidden" name="approved" id="approved" value="'.$freigabe.'"></input>
                                    <input type="hidden" name="public" id="public" value="'.$public_app.'"></input>
                                    <div class="form-group">
                                <label for="stand">Bearbeitungsstand</label>
                                <select name="stand" id="stand" class="form-control">
                                    <option value="'.$stand.'" selected>'.bearbeitungsstand_edit($stand).' (aktuell)</option>
                                    <option value="1">In Bearbeitung</i></option>
                                    <option value="2">Erledigt</i></option>
                                </select> 
                            </div>';
                                }

                            

                            echo '

                            <input type="hidden" name="terminid" id="terminid" value="'.$id.'">

                            <div class="form-group">
                            <center>
                            <button type="submit" class="btn btn-success" name="edit_termin" id="edit_termin">Termin aktualisieren
                            </button>
                            </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
}

}

if(isset($_POST['edit_termin'])){
    edit_termin($_POST['terminid'],$_POST['date'],$_POST['title'],htmlspecialchars($_POST['text']),$_POST['assignation'],$_POST['public'],$_POST['approved'],$_POST['category'],$_POST['stand']
);
}
/*
Not used by ok
function check($string) {
    $start =strpos($string, '<');
    $end  =strrpos($string, '>',$start);
  
    $len=strlen($string);
  
    if ($end !== false) {
      $string = substr($string, $start);
    } else {
      $string = substr($string, $start, $len-$start);
    }
    libxml_use_internal_errors(true);
    libxml_clear_errors();
    $xml = simplexml_load_string($string);
    return count(libxml_get_errors())==0;
  }
  */
 function validatehtml($html)
    {
        $curlHandler = curl_init('https://validator.nu/?out=gnu');

        curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $html);
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandler, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($curlHandler, CURLOPT_HTTPHEADER, [
            'Content-type: text/html; charset=utf-8',
            'Content-Length: ' . strlen($html),
        ]);

        $result = curl_exec($curlHandler);

        if (curl_getinfo($curlHandler, CURLINFO_HTTP_CODE) !== 200) {
            throw new RuntimeException('Curl error (are you connected to Internet?): ' . curl_error($curlHandler));
        }

        return $result;
    }

function edit_termin($id,$datum,$title,$text,$assignation,$public,$approved,$category,$stand){
    global $pdo;
    global $blr_user;

    $add_datum = strtotime($datum);
    $add_title = $title;
    $add_assignation = $assignation;
    $add_public = $public;
    $add_approved = $approved;
    $add_category = $category;
    $add_stand = $stand;
    $add_text = $text;
    //Research
    //$text = $text." <";
    $text_combined = "<!doctype html><html><head><title>x</title></head><body>".$text."</body></html>";// add this code have purpose to mark this is a html code
    $validate = validatehtml($text_combined);
    if(trim($validate) == ''){// if no errors
        

        $add = $pdo->prepare("UPDATE termine SET datum = '$add_datum', title = '$add_title', text = '$add_text', assignation = '$add_assignation', public = '$add_public', approved = '$add_approved', category = '$add_category', bearbeitungsstand = '$add_stand' WHERE id = $id");
        $add->execute();
        echo '<div class="alert alert-primary" role="alert">'."<strong>Update data success.</strong>".
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button></div>';
    }else{
        echo '<div class="alert alert-danger" role="alert">'."<strong>Update data failed.</strong><br/>".$validate.
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button></div>';
    }
    //echo "x".$val."x";
    /*
    not used by ok
    libxml_use_internal_errors(true);
    $dom = New DOMDocument();
    $dom->loadHTML($text);
    if (empty(libxml_get_errors())) {
      echo "This is a good HTML";
    }else {
      echo "This not html";
    }
    */
   // check($text);
    
    //End Research
    
    


}

function deleteTermin($terminID){
    global $pdo;

    $do = $pdo->prepare("UPDATE termine SET trash = '1' WHERE id = '$terminID'");
    $do->execute();

    echo '<center><h1>Der Termin wurde erfolgreich gelöscht!</h1></center><br>
    <center><h2>Versehentlich gelöscht? <a href="/undelete/'.$terminID.'">Rückgängig machen</a></h2></center>';
}

function undeleteTermin($terminID){
    global $pdo;

    $do = $pdo->prepare("UPDATE termine SET trash = '0' WHERE id = '$terminID'");
    $do->execute();

    echo '<center><h1>Der Termin wurde wiederhergestellt!</h1></center><br>
    <center><h2>Versehentlich wiederhergestellt? <a href="/delete/'.$terminID.'">Rückgängig machen</a></h2></center>';
}


function print_view(){
    global $pdo;
    global $trans;

    $timestamp_now = time();
    $today = strtotime('-1 day', $timestamp_now);
    $max_time = strtotime('+14 day', $timestamp_now);
    

    $get = $pdo->prepare("SELECT * FROM termine WHERE datum >= $today AND datum <= $max_time AND trash != '1' ORDER BY datum ASC");
    $get->execute();

    while($data = $get->fetch()){  
        $id = $data['id'];
        $creator = get_user_name($data['creator']);
        $assigned_to = get_user_name($data['assignation']);
        $infotext = $data['text'];
        $stand = $data['bearbeitungsstand'];

    
        if($infotext != '&lt;br&gt;'){
            $text_text = '<tr >
            <td colspan="6"><div class="" id="termin'.$id.'">
            
              <div class="col-lg-12">'.html_entity_decode($infotext).'</div>
            </div> </td>
        </tr>';
        }else{
            $text_button = '';
            $text_toogle = '';
            $text_text = '';
        }
    

        $get_type = explode(',', $data['art']);
        foreach ($get_type as $type_id){
           $type = get_type($type_id);
        }


        $category = get_category($data['category']);
        $public_status = $data['public'];
        $approved_status = $data['approved'];
        $datum = $data['datum'];
        if($public_status == 'true' && $approved_status == 'true'){
            $show_status = '<i class="fa fa-2x fa-check-double" style="color:green" title="Öffentlich & Freigegeben"></i>';
        }else{
            $show_status = get_public_status($public_status).' '.get_approve_status($approved_status);
        }
        
        $get_date = $data['datum'];
        $full_date = date('d.m.Y H:i', $get_date);
        $date = date('d.m.Y', $data['datum']);
        $time = date('H:i', $data['datum']);
        if($time == '00:00'){
            $timevalue = strtr(date("l", $get_date), $trans).', '.date('d.m.Y', $get_date).'';
        }else{
            $timevalue = strtr(date("l", $get_date), $trans).', '.date('d.m.Y H:i', $get_date).' Uhr';
        }
      
        echo '
        <tr '.$text_toogle.'>
          <td>
         
          '.bearbeitungsstand($stand).'
          '.$show_status.'
          </td>
            <td>'.$timevalue.'</td>
            <td>'.$data['title'].'</td>
            <td>'.$type.'</td>
            <td>'.$creator.'</td>
            <td>'.$assigned_to.'</td>
        </tr>
        
        '.$text_text;

    }
}

?>