<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="container">
                <div class="card mt-5">
                    <div class="card-body">
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="mail">
                                E-Mail Adresse
                                </label>
                                <input type="text" class="form-control" name="mail" id="mail" required>
                            </div>

                            <div class="form-group">
                                <label for="password">
                                Passwort
                                </label>
                                <input type="password" class="form-control" name="password" id="password" required>
                            </div>

                            <div class="form-group">
                            <center>
                            <button type="submit" class="btn btn-primary" name="logmein" id="logmein">Login</button>
                            </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>