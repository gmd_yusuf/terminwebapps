<?php
$timepickdate = date('d-m-Y').' 00:00';
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="container">
                <div class="card mt-5">
                    <div class="card-body">
                        <form action="/dashboard" method="post">
                            <div class="form-group">
                                <label for="title">
                                Titel
                                </label>
                                <input type="text" class="form-control" name="title" id="title">
                            </div>

                            <div class="form-group">
                            <label for="datetimepicker">Datum</label>
                <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" name="date" id="datetimepicker" data-toggle="datetimepicker" data-target="#datetimepicker" value="<?php echo $timepickdate?>"/>
                   
                </div>
            </div>

                            <div class="form-group">
                                <label for="assignation">Zuweisung</label>
                                <select name="assignation" class="form-control" id="assignation">
                                <?php get_users_select();?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="art">Termintyp / Verarbeitungsform</label>
                                <select name="art[]" class="form-control" multiple="multiple" id="art">
                                <?php get_types_select();?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="assignation">Kategorie</label>
                                <select name="category" class="form-control" id="category">
                                <?php get_categories_select();?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="text">Termindetails</label>
                                <textarea name="text" id="text" style="width: 100%;height: 300px;margin: 0;padding: 0;border-width: 0; "></textarea>                            
                            </div>

                            <?php
                            // nur admin Rechte
                            if($blr_user['is_admin'] == 'true'){
                                
                            echo '<div class="form-group">
                                <label for="approved">Freigabe</label>
                                <select name="approved" id="approved" class="form-control">
                                    <option value="false">Nicht freigegeben</option>
                                    <option value="true">Freigegeben</option>
                                </select> 
                            </div>
                            <div class="form-group">
                                <label for="public">Öffentliche Anzeige</label>
                                <select name="public" id="public" class="form-control">
                                    <option value="false">Nicht öffentlich</option>
                                    <option value="true">Öffentlich</option>
                                </select> 
                            </div>';
                            }else{
                                echo '
                                <input type="hidden" name="approved" id="approved" value="false"></input>
                                <input type="hidden" name="public" id="public" value="false"></input>
                                ';
                            }
                            ?>

                            <div class="form-group">
                                <label for="stand">Bearbeitungsstand</label>
                                <select name="stand" id="stand" class="form-control">
                                    <option value="0">Offen</font></option>
                                    <option value="1">In Bearbeitung</i></option>
                                    <option value="2">Erledigt</i></option>
                                </select> 
                            </div>

                            <div class="form-group">
                            <center>
                            <button type="submit" class="btn btn-success" name="add_termin" id="add_termin">Termin hinzufügen</button>
                            </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>