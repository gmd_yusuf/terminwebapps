<?php


include_once 'core/functions.php';

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Terminvorschau &bull; BLR</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/97d45560a5.js" crossorigin="anonymous"></script>
</head>
<body>
    



<!-- Temporäre Anzeige -->

<div class="container-fluid">
  <div class="row">
    

    <div class="col-lg-12">

    
            <div class="mt-3">
                <div class="card">
                    <div class="card-header text-center">
                    <p class="display-4">Information</p>
                    </div>

                    <div class="card-body text-center">
                        
                        <p class="card-text">
                        Die Terminvorschau steht Ihnen in Kürze wieder zur Verfügung.<br><br>

                        <b>Sie haben einen Termin für uns?</b><br>
                        Gerne erreichen Sie uns jederzeit per E-Mail an <a href="mailto:cvd@blr.de">cvd@blr.de</a> oder rufen Sie uns unter der <a href="tel:08949994510">+49 (0) 89 49994 510</a> an.<br>
                        <br>
                        <i>Ihre BLR-CvDs</i>
                        </p>
                            </div>
                        <p></p>
                    </div>

                    
                    
                       
                </div>
            </div>
        
           
        
    
  
    </div>


  </div>
</div>

<!-- Ende temporäre Anzeige -->



</body>
</html>