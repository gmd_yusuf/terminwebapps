<nav class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
  <!-- Brand -->
  <a class="navbar-brand" href="/dashboard"><img src="/img/logo.png" height="50px" width="auto" alt=""> | Themen & Termine</a>


  <div>
    Hallo, <b><?php echo $blr_user['surname'] . ' ' .$blr_user['name'];?></b>! 
    | 
    <a href="/logout" title="Logout"><button class="btn btn-danger"><i class="fa fa-lock"></i></button></a>
    <a href="/add"><button class="btn btn-success"><i class="fa fa-plus-circle"></i></button></a>
    <a href="/print" target="_blank"><button class="btn btn-primary"><i class="fa fa-print"></i></button></a>
  </div>


 
</nav>